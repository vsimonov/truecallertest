package com.truecaller.vvs;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.google.common.base.Function;
import com.google.common.eventbus.Subscribe;

import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import static com.truecaller.vvs.ServiceLocator.eventBus;
import static com.truecaller.vvs.ServiceLocator.network;


public class MainActivity extends ActionBarActivity {

    private static final String WORD_TO_COUNT = "search";
    private static final int N = 10; // in real life constant would have more sensible name

    private TextView textNthChar;
    private TextView textEveryNthChar;
    private TextView textWordCounter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textNthChar = (TextView) findViewById(R.id.textNthChar);
        textEveryNthChar = (TextView) findViewById(R.id.textEveryNthChar);
        textWordCounter = (TextView) findViewById(R.id.textWordCounter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    protected void onStart() {
        super.onStart();
        eventBus().register(this);
        displayTasksState();
    }

    @Override
    protected void onStop() {
        eventBus().unregister(this);
        super.onStop();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_load:
                network().requestNthChar(N);
                network().requestEveryNthChar(N);
                network().requestWordCounter();
                displayTasksState();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Subscribe
    @SuppressWarnings("unused")
    public void on(Network.GetNthCharFinished event) {
        updateNthCharTaskState();
    }

    @Subscribe
    @SuppressWarnings("unused")
    public void on(Network.GetEveryNthCharFinished event) {
        updateEveryNthCharTaskState();
    }

    @Subscribe
    @SuppressWarnings("unused")
    public void on(Network.GetWordCounterFinished event) {
        updateWordCountState();
    }

    private void displayTasksState() {
        updateNthCharTaskState();
        updateEveryNthCharTaskState();
        updateWordCountState();
    }

    private void updateNthCharTaskState() {
        displayRequestState(
                network().getNthCharRequest(),
                textNthChar,
                new Function<Character, String>() {
                    @Override
                    public String apply(Character character) {
                        return getString(R.string.textNthChar, N, character);
                    }
                });
    }

    private void updateEveryNthCharTaskState() {
        displayRequestState(
                network().getEveryNthCharRequest(),
                textEveryNthChar,
                new Function<String, String>() {
                    @Override
                    public String apply(String symbols) {
                        return getString(R.string.textEveryNthChar, N, symbols);
                    }
                });
    }

    private void updateWordCountState() {
        displayRequestState(
                network().getWordCountRequest(),
                textWordCounter,
                new Function<Map<String, int[]>, String>() {
                    @Override
                    public String apply(Map<String, int[]> counts) {
                        int[] countReference = counts.get(WORD_TO_COUNT);
                        int count = countReference == null ? 0 : countReference[0];
                        return getResources().getQuantityString(R.plurals.textNumWordOccurrences, count, WORD_TO_COUNT, count);
                    }
                });
    }

    private <T>  void displayRequestState(Future<T> request, TextView view, Function<T, String> transform) {
        if (request == null) {
            view.setText(R.string.textNoInfo);
        } else if (request.isDone()) {
            try {
                String text = transform.apply(request.get());
                view.setText(text);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            } catch (ExecutionException e) {
                view.setText(getString(R.string.textError, e.getCause()));
            }
        } else {
            view.setText(R.string.textLoading);
        }
    }
}
