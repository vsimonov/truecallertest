package com.truecaller.vvs;

import android.app.Application;

public class TruecallerTestApplication extends Application {

    @Override
    public void onCreate() {
        ServiceLocator.initialize(this);
    }
}
