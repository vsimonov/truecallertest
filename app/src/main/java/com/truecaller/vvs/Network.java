package com.truecaller.vvs;

import com.google.common.util.concurrent.ListenableFuture;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.regex.Pattern;

import static com.google.common.util.concurrent.MoreExecutors.sameThreadExecutor;
import static com.truecaller.vvs.ServiceLocator.background;
import static com.truecaller.vvs.ServiceLocator.eventBus;

public class Network {
    private static final int CONNECTION_TIMEOUT = 10 * 1000;
    private static final int SOCKET_TIMEOUT = 10 * 1000;
    private static final String TRUECALLER_URL = "http://truecaller.com";

    private ListenableFuture<Character> nthCharRequest;
    private ListenableFuture<String> everyNthCharRequest;
    private ListenableFuture<Map<String, int[]>> wordCounterRequest;

    public synchronized Future<Character> requestNthChar(final int n) {
        if (nthCharRequest == null || nthCharRequest.isDone()) {
            nthCharRequest = background().submit(new Callable<Character>() {
                @Override
                public Character call() throws Exception {
                    return getNthChar(n);
                }
            });
            postWhenFinished(new GetNthCharFinished() { }, nthCharRequest);
        }
        return nthCharRequest;
    }

    private void postWhenFinished(final Object event, ListenableFuture<?> f) {
        f.addListener(new Runnable() {
            @Override
            public void run() {
                eventBus().post(event);
            }
        }, sameThreadExecutor());
    }

    public synchronized  Future<Character> getNthCharRequest() {
        return nthCharRequest;
    }

    public synchronized Future<String> requestEveryNthChar(final int n) {
        if (everyNthCharRequest == null || everyNthCharRequest.isDone()) {
            everyNthCharRequest = background().submit(new Callable<String>() {
                @Override
                public String call() throws Exception {
                    return getEveryNthChar(n);
                }
            });
            postWhenFinished(new GetEveryNthCharFinished() { }, everyNthCharRequest);
        }
        return everyNthCharRequest;
    }

    public synchronized Future<String> getEveryNthCharRequest() {
        return everyNthCharRequest;
    }

    public synchronized Future<Map<String, int[]>> requestWordCounter() {
        if (wordCounterRequest == null || wordCounterRequest.isDone()) {
            wordCounterRequest = background().submit(new Callable<Map<String, int[]>>() {
                @Override
                public Map<String, int[]> call() throws Exception {
                    return getWordCounter();
                }
            });
            postWhenFinished(new GetWordCounterFinished() {
            }, wordCounterRequest);
        }
        return wordCounterRequest;
    }

    public synchronized Future<Map<String, int[]>> getWordCountRequest() {
        return wordCounterRequest;
    }

    private char getNthChar(int n) throws IOException {
        Reader reader = getPageReader(TRUECALLER_URL);
        try {
            int c = readNthChar(reader, n);
            if (c == -1) {
                throw new IOException("Unexpected end of content");
            }
            return (char) c;
        } finally {
            reader.close();
        }
    }

    private String getEveryNthChar(int n) throws IOException {
        Reader reader = getPageReader(TRUECALLER_URL);
        try {
            StringBuilder builder = new StringBuilder();
            int c;
            while ((c = readNthChar(reader, n)) != -1) {
                builder.append((char) c);
            }
            return builder.toString();
        } finally {
            reader.close();
        }
    }

    private Map<String, int[]> getWordCounter() throws IOException {
        BufferedReader reader = new BufferedReader(getPageReader(TRUECALLER_URL));
        try {
            // Using int[] as simple and high performance reference to int.

            // There is no significant performance gain (only 0.01sec on Nexus 5
            // @see com.truecaller.vvs.TreeMapVsHashMapPerformanceTest) from using HashMap instead of TreeMap
            // comparing to text download time. Also it is complicated to maintain the same contract in
            // HashMap as TreeMap<String>(String.CASE_INSENSITIVE_ORDER).
            Map<String, int[]> result = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);
            // String.split uses regex pattern inside.
            // It is more efficient to compile pattern once and reuse it in a loop.
            Pattern whitespacePattern = Pattern.compile("\\s");
            while (true) {
                String line = reader.readLine();
                if (line == null) break;

                // page code is formatted with Tab, spaces etc. Simple split by space
                // produces bad result. So, I decided to split string on any whitespace symbol
                for (String word : whitespacePattern.split(line, 0)) {
                    int[] count = result.get(word);
                    if (count != null) {
                        count[0]++;
                    } else {
                        result.put(word, new int[] { 1 });
                    }
                }
            }
            return result;
        } finally {
            reader.close();
        }
    }

    private static int readNthChar(Reader reader, int n) throws IOException {
        return (reader.skip(n - 1) == n - 1) ? reader.read() : -1;
    }

    private static Reader getPageReader(String url) throws IOException {
        BasicHttpParams params = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(params, CONNECTION_TIMEOUT);
        HttpConnectionParams.setSoTimeout(params, SOCKET_TIMEOUT);

        HttpClient client = new DefaultHttpClient(params);
        HttpGet request = new HttpGet(url);
        HttpResponse response = client.execute(request);

        HttpEntity entity = response.getEntity();
        Header contentEncoding = entity.getContentEncoding();
        String charset = contentEncoding != null ? contentEncoding.getValue(): "UTF-8";

        return new InputStreamReader(entity.getContent(), charset);
    }

    /**************************************************************************
     *                          Event types
     *************************************************************************/

    // I like to use interfaces for declaring event types as it short, and then in debugger you
    // can determine what class event has came from by its class name: EventSender$AnonymousInnerClass
    public interface GetNthCharFinished { }

    public interface GetEveryNthCharFinished { }

    public interface GetWordCounterFinished { }
}
