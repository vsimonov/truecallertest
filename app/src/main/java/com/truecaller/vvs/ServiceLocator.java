package com.truecaller.vvs;

import android.os.Handler;

import com.google.common.eventbus.AsyncEventBus;
import com.google.common.eventbus.EventBus;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;

import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

// Used to access application-wide objects
public class ServiceLocator {

    private static Network network;
    private static TruecallerTestApplication application;
    private static ListeningExecutorService backgroundExecutor;
    private static EventBus eventBus;
    private static boolean isInitialized;

    public static void initialize(TruecallerTestApplication application) {
        assert !isInitialized;
        ServiceLocator.application = application;
        network = new Network();
        backgroundExecutor = MoreExecutors.listeningDecorator(Executors.newCachedThreadPool());

        Executor mainThreadExecutor = new Executor() {
            final Handler mainThreadHandler = new Handler();
            @Override
            public void execute(Runnable command) {
                mainThreadHandler.post(command);
            }
        };
        eventBus = new AsyncEventBus(mainThreadExecutor);
        isInitialized = true;
    }

    public static TruecallerTestApplication application() {
        // I know that assertions are disabled by default. It is used to document
        // my assumptions about code.
        assert isInitialized;
        return application;
    }

    public static ListeningExecutorService background() {
        assert isInitialized;
        return backgroundExecutor;
    }

    public static EventBus eventBus() {
        assert isInitialized;
        return eventBus;
    }

    public static Network network() {
        assert isInitialized;
        return network;
    }

    private ServiceLocator() {
        // forbid instance creation
    }
}
