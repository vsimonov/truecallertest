package com.truecaller.vvs;

import android.test.AndroidTestCase;
import android.util.Log;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

// I was curious about how much in this particular case HashMap will be faster comparing to
// TreeMap. As HashMap takes just O(1) for search, but should resize underlying array and uses
// hashCode and equals. TreeMap, on the other hand will not be very high (11 - for ~1500 elements),
// will use lightweight comparison, but have to rebalance itself on inserts.
public class TreeMapVsHashMapPerformanceTest extends AndroidTestCase {

    String[] words = new String[]{
            "<!DOCTYPE", "html>", "<html", "lang=\"en\"", "dir=\"ltr\"", "class=\"ltr\">",
            "<head>", "", "", "", "", "", "", "", "<meta", "charset=\"UTF-8\">", "", "",
            "<meta", "http-equiv=\"X-UA-Compatible\"", "content=\"IE=edge,chrome=1\">", "",
            "", "", "", "", "", "", "", "<meta", "name=\"viewport\"", "content=\"width=device-width,",
            "initial-scale=1,", "maximum-scale=1.0,", "user-scalable=no\">", "", "", "<meta",
            "name=\"description\"", "content=\"Truecaller", "is", "transforming", "today’s",
            "phonebook", "to", "make", "it", "more", "intelligent", "and", "useful.​\">", "",
            "", "<meta", "name=\"inmobi-site-verification\"", "content=\"1bed864abfbc5b2b838ca0bfd6a4331\">",
            "", "", "<meta", "name=\"inmobi-site-verification\"", "content=\"b39d1eac959358f0a6d55934550d67e1\">",
            "", "", "<meta", "name=\"twitter:card\"", "content=\"app\">", "", "", "<meta",
            "name=\"twitter:app:id:iphone\"", "content=\"448142450\">", "", "", "<meta",
            "name=\"twitter:app:id:ipad\"", "content=\"448142450\">", "", "", "<meta",
            "name=\"twitter:app:id:googleplay\"", "content=\"com.truecaller\">", "", "",
            "<link", "href=\"https://plus.google.com/+truecaller\"", "rel=\"publisher\">", "",
            "", "<link", "rel=\"shortcut", "icon\"", "href=\"/favicon.ico\"", "type=\"image/x-icon\">",
            "", "", "", "", "<meta", "property=\"og:type\"", "content=\"website\">", "", "",
            "<meta", "property=\"og:locale\"", "content=\"en\">", "", "", "<meta", "property=\"og:url\"",
            "content=\"http://www.truecaller.com\">", "", "", "<meta", "property=\"og:title\"",
            "content=\"Truecaller", "|", "Phone", "Number", "Search\">", "", "", "<meta",
            "property=\"og:image\"", "content=\"http://www.truecaller.com/img/press/truecaller-app-icon-android.png\">",
            "", "", "<meta", "property=\"og:description\"", "content=\"Truecaller", "is",
            "transforming", "today’s", "phonebook", "to", "make", "it", "more", "intelligent",
            "and", "useful.​\">", "", "", "", "", "<meta", "name=\"format-detection\"",
            "content=\"telephone=no\">", "", "<title>Phone", "Number", "Search", "|",
            "Truecaller</title>", "", "<script>window.Truecaller", "=", "{};</script>",
            "<link", "href='//fonts.googleapis.com/css?family=Open+Sans:300,400,400italic,600'",
            "rel='stylesheet'", "type='text/css'>", "", "<link", "media=\"all\"", "type=\"text/css\"",
            "rel=\"stylesheet\"", "href=\"//www.truecaller.com/css/main-stylus.1425481382.css\">",
            "", "<script", "src=\"/js/modernizr.custom.16777.js\"></script>", "<script>", "",
            "Modernizr.addTest('flexboxtweener',", "Modernizr.testAllProps('flexAlign',",
            "'end',", "true));", "</script>", "<!--[if", "lt", "IE", "9]>", "", "<script",
            "src=\"/js/IE9.js\"></script>", "", "<script", "src=\"/js/respond.js\"></script>",
            "", "<![endif]-->", "", "<script>", "", "var", "_prum", "=", "[['id',", "'54251360abe53dba138f6996'],",
            "['mark',", "'firstbyte',", "(new", "Date()).getTime()]];", "", "(function()",
            "{", "", "", "", "", "", "var", "s", "=", "document.getElementsByTagName('script')[0]",
            "", "", "", "", "", "", "", ",", "p", "=", "document.createElement('script');",
            "", "", "", "", "", "p.async", "=", "'async';", "", "", "", "", "", "p.src", "=",
            "'//rum-static.pingdom.net/prum.min.js';", "", "", "", "", "", "s.parentNode.insertBefore(p,",
            "s);", "", "})();", "", "</script>", "</head>", "<body", "class=\"site\">", "",
            "<div", "class=\"modal-mask\"></div>", "<p", "class=\"browserupgrade\">You",
            "are", "using", "an", "<strong>outdated</strong>", "browser.", "Please", "<a",
            "href=\"http://browsehappy.com/\">upgrade", "your", "browser</a>", "to",
            "improve", "your", "experience.</p>", "<div", "class=\"preloader\">", "", "<div",
            "class=\"preloader__container\">", "", "", "<span></span>", "", "", "<span></span>",
            "", "", "<span></span>", "", "", "<span></span>", "", "", "<span></span>", "",
            "</div>", "</div>", "<header>", "", "", "<nav", "class=\"navbar", "navbar--desktop\">",
            "", "<div", "class=\"navbar__inner\">", "", "", "<div", "class=\"navbar__logo\">",
            "", "", "", "<a", "data-href=\"http://www.truecaller.com\">", "", "", "", "",
            "<img", "src=\"/img/truecaller-logo.png\"", "width=\"100px\"", "height=\"20px\">",
            "", "", "", "</a>", "", "", "</div>", "", "", "<div", "class=\"navbar__menu\">",
            "", "", "", "<ul>", "", "", "", "", "", "", "", "", "<li><a", "data-href=\"http://www.truecaller.com/download\">Download</a></li>",
            "", "", "", "", "<li", "class=\"navbar__menu--dropdown\">", "", "", "", "", "",
            "<ul>", "", "", "", "", "", "", "<li><a", "class=\"navbar__item-with-image\"",
            "dir=\"ltr\"", "data-href=\"http://www.truecaller.com/services/truecaller\"><img",
            "width=\"18px\"", "src=\"/img/services/truecaller/truecaller-app-icon-blue.png\">",
            "Truecaller</a></li>", "", "", "", "", "", "", "<li><a", "class=\"navbar__item-with-image\"",
            "dir=\"ltr\"", "data-href=\"http://www.truecaller.com/services/truedialer\"><img",
            "width=\"18px\"", "src=\"/img/services/truedialer/truedialer-app-icon-blue.png\">",
            "Truedialer</a></li>", "", "", "", "", "", "</ul>", "", "", "", "", "", "<a",
            "data-href=\"#\">Products", "<span", "class=\"arrow", "arrow--down\"></span></a>",
            "", "", "", "", "</li>", "", "", "", "", "<li><a", "data-href=\"http://www.truecaller.com/support\">Support</a></li>",
            "", "", "", "", "", "", "", "", "", "<li><a", "data-href=\"#\"", "id=\"signInButton\"",
            "data-modal-load=\"sign-in-and-enhanced-search\"", "data-modal-url=\"http://www.truecaller.com/modals/sign-in-and-enhanced-search\">Sign",
            "In</a></li>", "", "", "", "</ul>", "", "", "</div>", "", "</div>", "</nav>",
            "<nav", "class=\"navbar", "navbar--mobile\">", "", "<div", "class=\"navbar__logo\">",
            "", "", "<a", "data-href=\"http://www.truecaller.com\">", "", "", "", "<img",
            "src=\"/img/truecaller-logo.png\"", "width=\"100px\"", "height=\"20px\">", "", "",
            "", "", "", "</a>", "", "</div>", "", "<div", "class=\"navbar__menu-button\"><span></span></div>",
            "", "<div", "class=\"navbar__menu\">", "", "", "<ul>", "", "", "", "<li>Get",
            "started</li>", "", "", "", "<li><a", "data-href=\"http://www.truecaller.com/download\">Download</a></li>",
            "", "", "", "", "", "", "", "<li><a", "data-href=\"#\"", "data-modal-url=\"http://www.truecaller.com/modals/sign-in-and-enhanced-search\"",
            "data-modal-load=\"sign-in-and-enhanced-search\">Sign", "In</a></li>", "", "",
            "</ul>", "", "", "", "<ul>", "", "", "", "<li>Products</li>", "", "", "",
            "<li><a", "data-href=\"http://www.truecaller.com/services/truecaller\">Truecaller</a></li>",
            "", "", "", "<li><a", "data-href=\"http://www.truecaller.com/services/truedialer\">Truedialer</a></li>",
            "", "", "</ul>", "", "", "<ul>", "", "", "", "<li>Help</li>", "", "", "",
            "<li><a", "data-href=\"http://www.truecaller.com/support\">Support</a></li>", "",
            "", "", "<li><a", "data-href=\"http://www.truecaller.com/contact\">Contact",
            "us</a></li>", "", "", "</ul>", "", "", "<ul>", "", "", "", "<li>About</li>", "",
            "", "", "<li><a", "data-href=\"http://www.truecaller.com/about\">About</a></li>",
            "", "", "", "<li><a", "data-href=\"http://www.truecaller.com/press\">Press</a></li>",
            "", "", "", "<li><a", "data-href=\"http://www.truecaller.com/careers\">Careers</a></li>",
            "", "", "", "<li><a", "href=\"http://www.truecaller.com/blog\">Blog</a></li>", "",
            "", "", "<li><a", "data-href=\"http://www.truecaller.com/ambassadors\">Ambassadors</a></li>",
            "", "", "</ul>", "", "", "<ul>", "", "", "", "<li>Social</li>", "", "", "",
            "<li><a", "data-href=\"http://facebook.com/truecaller\">Facebook</a></li>", "",
            "", "", "<li><a", "data-href=\"https://plus.google.com/+truecaller\"", "rel=\"publisher\">Google+</a></li>",
            "", "", "", "<li><a", "data-href=\"http://twitter.com/truecaller\">Twitter</a></li>",
            "", "", "", "<li><a", "data-href=\"http://instagram.com/truecaller\">Instagram</a></li>",
            "", "", "", "<li><a", "data-href=\"http://www.linkedin.com/company/true-software-scandinavia-ab\">LinkedIn</a></li>",
            "", "", "</ul>", "", "", "<ul>", "", "", "", "<li>", "", "", "", "", "<small>",
            "", "", "", "", "", "<a", "onclick=\"window.open('http://www.truecaller.com/terms-of-service',",
            "'Terms", "of", "Service", "|", "Truecaller',", "'width=600,", "height=800');",
            "return", "false;\">Terms", "of", "service</a>", "", "", "", "", "</small>", "",
            "", "", "</li>", "", "", "", "<li>", "", "", "", "", "<small>", "", "", "", "",
            "", "<a", "onclick=\"window.open('http://www.truecaller.com/privacy-policy',",
            "'Privacy", "Policy", "|", "Truecaller',", "'width=600,", "height=800');",
            "return", "false;\">Privacy", "Policy</a>", "", "", "", "", "</small>", "", "",
            "", "</li>", "", "", "", "<li>", "", "", "", "", "<small>", "", "", "", "", "",
            "<a", "href=\"http://www.truecaller.com/sitemap\">Sitemap</a>", "", "", "", "",
            "</small>", "", "", "", "</li>", "", "", "", "<li>", "", "", "", "", "<small>",
            "", "", "", "", "", "<a", "href=\"http://www.truecaller.com/languages\">Languages</a>",
            "", "", "", "", "</small>", "", "", "", "</li>", "", "", "", "<li>", "", "", "",
            "", "<small>", "", "", "", "", "", "<a", "href=\"http://www.truecaller.com/spam-numbers\">Spam",
            "Numbers</a>", "", "", "", "", "</small>", "", "", "", "</li>", "", "", "</ul>",
            "", "", "<p", "class=\"copyright\">Copyright", "©", "2009-2015", "True",
            "Software", "Scandinavia", "AB.", "All", "rights", "reserved.", "Truecaller™",
            "is", "a", "registred", "trademark.<br>", "Responsible", "publisher:", "Alan",
            "Mamedi", "appointed", "by", "True", "Software", "Scandinavia", "AB.", "Database",
            "name:", "Truecaller.com</p>", "", "</div>", "</nav>", "</header>", "", "", "",
            "<nav>", "", "", "", "</nav>", "", "", "<main>", "", "", "<section", "class=\"page",
            "page--home\">", "", "<div", "class=\"truecaller-land\"></div>", "", "<div",
            "class=\"container", "container--medium\">", "", "", "<h1", "class=\"title--large\">Search",
            "among", "1.5", "billion", "phone", "numbers", "worldwide</h1>", "", "", "<div",
            "class=\"search-form\">", "", "<form", "action=\"http://www.truecaller.com/search\"",
            "method=\"POST\"", "autocomplete=\"off\"", "name=\"search_form\">", "", "",
            "<div", "class=\"search-form__number-field\">", "", "", "", "<input", "", "", "",
            "", "class=\"search-form__input\"", "", "", "", "", "name=\"phone_number\"", "",
            "", "", "", "data-validate=\"required|phone_number|min:4|max:20\"", "", "", "",
            "", "type=\"tel\"", "", "", "", "", "placeholder=\"Phone", "number\"", "", "", "",
            "", "value=\"\"", "", "", "", "", "autofocus>", "", "", "", "<button", "class=\"search-form__submit-button\"",
            "type=\"submit\"", "disabled>Search</button>", "", "", "</div>", "", "", "<div",
            "class=\"search-form__country-field\">", "", "", "", "<div", "", "", "", "",
            "class=\"select\"", "", "", "", "", "dir=\"ltr\"", "", "", "", "", "data-country-selector",
            "", "", "", "", "data-selected-country=\"RU\">", "", "", "", "", "<span>...</span>",
            "", "", "", "", "<select><option>...</option></select>", "", "", "", "</div>", "",
            "", "</div>", "", "</form>", "</div>", "", "", "", "", "<p", "class=\"align-center\">",
            "", "", "", "<a", "href=\"/blog/introducing-the-new-truecaller-for-iphone\"",
            "class=\"button", "button--medium", "button--white\"", ">new", "truecaller",
            "for", "iphone", "<span", "class=\"arrow", "arrow--right", "arrow--blue\"></span></a>",
            "", "", "</p>", "", "", "<p", "class=\"align-center\">", "", "", "", "<a",
            "href=\"#\"", "data-modal-load=\"truecaller-5-iphone-video\"", "data-modal-url=\"http://www.truecaller.com/modals/truecaller-5-iphone-video\"><img",
            "width=\"320px\"", "src=\"/img/home/truecaller-5-for-iphone-video-thumb.png\"></a>",
            "", "", "</p>", "", "</div>", "</section>", "", "</main>", "", "", "", "",
            "<footer>", "", "", "", "<nav>", "", "<div", "class=\"links\">", "", "", "<a",
            "data-href=\"http://www.truecaller.com/about\">About</a>", "", "", "<a",
            "data-href=\"http://www.truecaller.com/press\">Press</a>", "", "", "<a",
            "data-href=\"http://www.truecaller.com/careers\">Careers</a>", "", "", "<a",
            "href=\"http://www.truecaller.com/blog\">Blog</a>", "", "", "<a", "data-href=\"http://www.truecaller.com/contact\">Contact",
            "us</a>", "", "", "<a", "data-href=\"http://www.truecaller.com/ambassadors\">Ambassadors</a>",
            "", "</div>", "", "<button", "class=\"button", "button--white", "button--small\"",
            "data-modal-load=\"language\"", "data-modal-url=\"http://www.truecaller.com/modals/language\">",
            "", "", "English", "<span", "class=\"arrow", "arrow--down", "arrow--blue",
            "arrow--small\"></span>", "", "</button>", "</nav>", "<div", "class=\"copyright-and-social\">",
            "", "<p", "class=\"copyright", "copyright--footer\"", "dir=\"ltr\">", "", "",
            "Copyright", "©", "2009-2015", "True", "Software", "Scandinavia", "AB.", "All",
            "rights", "reserved.", "Truecaller™", "is", "a", "registred", "trademark.", "",
            "", "<br>", "", "", "Responsible", "publisher:", "Alan", "Mamedi", "appointed",
            "by", "True", "Software", "Scandinavia", "AB.", "Database", "name:", "Truecaller.com",
            "", "", "<br>", "", "", "<a", "href=\"http://www.truecaller.com/sitemap\">Sitemap</a>",
            "|", "", "", "<a", "href=\"http://www.truecaller.com/languages\">Languages</a>",
            "|", "", "", "<a", "onclick=\"window.open('/terms-of-service',", "'Terms", "of",
            "Service", "|", "Truecaller',", "'width=600,", "height=800,", "scrollbars=yes');",
            "return", "false;\">Terms", "of", "service</a>", "|", "", "", "<a", "onclick=\"window.open('/privacy-policy',",
            "'Privacy", "Policy", "|", "Truecaller',", "'width=600,", "height=800,",
            "scrollbars=yes');", "return", "false;\">Privacy", "Policy</a>", "|", "", "",
            "<a", "href=\"http://www.truecaller.com/spam-numbers\">Spam", "Numbers</a>", "",
            "</p>", "", "<p", "class=\"social\">", "", "", "<a", "data-href=\"http://facebook.com/truecaller\">Facebook</a>",
            "|", "", "", "<a", "data-href=\"https://plus.google.com/+truecaller\"", "rel=\"publisher\">Google+</a>",
            "|", "", "", "<a", "data-href=\"http://twitter.com/truecaller\">Twitter</a>", "|",
            "", "", "<a", "data-href=\"http://instagram.com/truecaller\">Instagram</a>", "|",
            "", "", "<a", "data-href=\"http://www.linkedin.com/company/true-software-scandinavia-ab\">LinkedIn</a>",
            "", "</p>", "", "</div>", "", "", "", "", "</footer>", "", "<script", "src=\"//www.truecaller.com/js/main.1425302102.js\"></script>",
            "", "", "<script>", "", "$(document).on('NoConnectionEvent',", "function(){",
            "Modal.open(\"no-connection\",", "\"http://www.truecaller.com/modals/no-connection\");",
            "});", "", "if(", "Truecaller", "&&", "Truecaller.sign_in", "===", "'init')",
            "Modal.open(\"sign-in-and-enhanced-search\",", "\"http://www.truecaller.com/modals/sign-in-and-enhanced-search\");",
            "", "if(", "Truecaller", "&&", "Truecaller.sign_in_match", "===", "'init')",
            "Modal.open(\"sign-in-and-enhanced-search-match\",", "\"http://www.truecaller.com/modals/sign-in-and-enhanced-search-match\");",
            "", "if(", "Truecaller", "&&", "Truecaller.no_connection", "===", "'init'", ")",
            "Modal.open(\"no-connection\",", "\"http://www.truecaller.com/modals/no-connection\");",
            "", "if(", "Truecaller", "&&", "Truecaller.match_found", "===", "'init'", ")",
            "Modal.open(\"match-found\",", "\"http://www.truecaller.com/modals/match-found\");",
            "", "if(", "Truecaller", "&&", "Truecaller.enhanced_search", "===", "'init'", ")",
            "Modal.open(\"enhanced-search\",", "\"http://www.truecaller.com/modals/enhanced-search\");",
            "", "if(", "Truecaller", "&&", "Truecaller.limit_exceeded", "===", "'init')",
            "Modal.open(\"limit-exceeded\",", "\"http://www.truecaller.com/modals/limit-exceeded\");",
            "", "if(", "Truecaller", "&&", "Truecaller.unsubscribe_newsletter", "===",
            "'init')", "Modal.open(\"unsubscribe-newsletter\",", "\"http://www.truecaller.com/modals/unsubscribe-newsletter\");",
            "", "if(", "Truecaller", "&&", "Truecaller.android_install", "===", "'init')",
            "Modal.open(\"android-apk\",", "\"http://www.truecaller.com/modals/android-apk\");",
            "</script>", "<script>", "", "", "(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){",
            "", "(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new", "Date();a=s.createElement(o),",
            "", "m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)",
            "", "})(window,document,'script','//www.google-analytics.com/analytics.js','ga');",
            "", "", "ga('create',", "'UA-3261967-7',", "'auto');", "", "ga('send',",
            "'pageview');", "", "", "var", "trackEvent", "=", "function(el,", "category,",
            "action){", "", "", "ga('send',", "'event',", "{", "", "", "", "'eventCategory':",
            "category,", "", "", "", "'eventAction':", "action,", "", "", "", "'hitCallback':",
            "function(){", "", "", "", "", "if(el", "&&", "el.dataset.url)", "", "", "", "",
            "", "window.location.href", "=", "el.dataset.url;", "", "", "", "}", "", "",
            "});", "", "}", "", "var", "trackPageView", "=", "function(el,", "page,",
            "title){", "", "", "ga('send',", "'pageview',", "{", "", "", "", "'page':",
            "page,", "", "", "", "'title':", "title,", "", "", "", "'hitCallback':",
            "function(){", "", "", "", "", "if(el", "&&", "el.dataset.url)", "", "", "", "",
            "", "window.location.href", "=", "el.dataset.url;", "", "", "", "}", "", "",
            "});", "", "}", "", "", "$(", "document", ")", "", "", ".on('SignInEvent',",
            "function(event,", "data){", "", "", "", "if(data.provider", "==", "'google')",
            "provider", "=", "'Google';", "", "", "", "if(data.provider", "==", "'facebook')",
            "provider", "=", "'Facebook';", "", "", "", "if(data.provider", "==", "'yahoo')",
            "provider", "=", "'Yahoo';", "", "", "", "if(data.provider", "==", "'windows-live')",
            "provider", "=", "'Windows", "Live';", "", "", "", "ga('send',", "'event',",
            "'User',", "'Sign-in',", "provider);", "", "", "", "if($('.page--search').length",
            ">", "0)", "", "", "", "", "trackPageView(null,", "'/pageview/search-result-signed-in',",
            "'Search", "Result", "Signed", "In');", "", "", "})", "", "", ".on('SearchNotSignedInEvent',",
            "function(event,", "data){", "ga('send',", "'event',", "'User',", "'Search", "-",
            "Not", "Signed", "In');", "})", "", "", ".on('SearchEvent',", "function(event,",
            "data){", "ga('send',", "'event',", "'User',", "'Search", "-", "Signed", "In');",
            "});", "</script></body>", "</html>"
    };

    public void testPerformance() {
        // warm up
        getAverageRunningTimeMs(createTreeMapBuilder(), 50);
        getAverageRunningTimeMs(createHashMapBuilder(), 50);
        // measure
        double t1 = getAverageRunningTimeMs(createTreeMapBuilder(), 100);
        double t2 = getAverageRunningTimeMs(createHashMapBuilder(), 100);
        // print result in error message
        assertTrue(String.format("fill TreeMap takes %.3fms fill HashMap takes %.3fms", t1, t2), false);
    }

    private double getAverageRunningTimeMs(MapBuilder builder, int numAttempts) {
        long t = System.nanoTime();
        int n = 0;
        for (int i = 0; i < numAttempts; i++) {
            Map<String, int[]> map = builder.build();
            n += map.size();
        }
        long time = System.nanoTime() - t;
        Log.d("Test", "" + n); // to prevent compiler from throwing our tree away
        return time / 1000000.0 / numAttempts;
    }

    private MapBuilder createTreeMapBuilder() {
        return new MapBuilder() {
            @Override
            public Map<String, int[]> build() {
                TreeMap<String, int[]> tree = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);
                for (String w : words) {
                    int[] count = tree.get(w);
                    if (count != null) {
                        count[0]++;
                    } else {
                        tree.put(w, new int[] {1});
                    }
                }
                return tree;
            }
        };
    }

    private MapBuilder createHashMapBuilder() {
        return new MapBuilder() {
            @Override
            public Map<String, int[]> build() {
                HashMap<String, int[]> hashMap = new HashMap<>();
                for (String w : words) {
                    String key = w.toLowerCase();
                    int[] count = hashMap.get(key);
                    if (count != null) {
                        count[0]++;
                    } else {
                        hashMap.put(key, new int[]{1});
                    }
                }
                return hashMap;
            }
        };
    }

    private interface MapBuilder {
        Map<String, int[]> build();
    }
}